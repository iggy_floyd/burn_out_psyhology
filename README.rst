================================================================
An assey on the topic "Burn-Out in psyhology"
================================================================





:Author: Igor Marfin
:Contact: igor.marfin@unister.de
:Organization: private
:Date: Oct 28, 2015, 9:28:51 AM
:Status: draft
:Version: 1
:Copyright: This document has been placed in the public domain. You
            may do with it as you wish. You may copy, modify,
            redistribute, reattribute, sell, buy, rent, lease,
            destroy, or improve it, quote it at length, excerpt,
            incorporate, collate, fold, staple, or mutilate it, or do
            anything else to it that your or anyone else's heart
            desires.

.. admonition:: Dedication

    For psychologists.

.. admonition:: Abstract

    This  project contains the assey written in German about the "Burn-Out" problem in the psychology.

.. meta::
   :keywords: german, burn-out
   :description lang=en:  The assey  about the problem of the "Burn-Out"




.. contents:: Table of Contents







----------------------------------
Installation of the package
----------------------------------


.. Important::
  
    The project relies on the presence of ``Autotools`` in the system.
    Before, proceed further, please, install them: ``sudo apt-get install autotools-dev``.

Simply, clone the project 

.. code-block:: 

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/burn_out_psyhology.git

and test a configuration of your system that you have all components installed:
    
* python 

* qpdf

In order to do this, you can just run the default rule of  ``Makefile`` via the 
command in the shell:

.. code-block:: 
    
    make

    

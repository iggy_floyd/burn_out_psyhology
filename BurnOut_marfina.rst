
.. role:: raw-tex(raw)
    :format: latex html



.. default-role:: raw-tex


================================================================
 Burn-Out-Syndrom
================================================================



:Autor: Julia Marfina
:Kontakt: yuliya.marfina@gmail.com
:Datum: Oct 28, 2015, 9:28:51 AM
:Version: 1
:Link zum Herunterladen: https://bitbucket.org/iggy_floyd/burn_out_psyhology

.. figure:: figs/burnout_1708025.jpg
   :align: center
   :width: 770px
   :height: 520px
   :scale: 100
   :alt: Burn-Out




.. admonition:: Zusammenfassung

    Für den Begriff Burn-Out existieren unterschiedliche Bezeichnungen und Schreibweisen: 
    Burn-Out, Burnout, Burnoutsyndrom, BurnOut-Syndrom. 
    Der Begriff kommt aus dem Englischen und bedeutet "ausbrennen" oder "ausgebrannt sein". 
    Gemeint ist ein berufsbezogenes Erschöpfungssyndrom.



.. meta::
   :keywords: Burn-Out,Burnout,Erschöpfungssyndrom.
   :description lang=en: A German assey on the Burnout topic in the psychology.





.. contents:: Inhaltsverzeichnis 




.. section-numbering::








.. raw:: pdf

   PageBreak cutePage




----------------------------------------------------------------
 Was  ist  Burn-Out?
----------------------------------------------------------------


Bisher ist das Burn-Out-Syndrom medizinisch nur unzureichend erforscht. 
So existiert bisher keine medizinisch verbindliche Definition noch ein allgemeingültiges Instrument für die Diagnostik. 
Durch diese Unsicherheit wird der Begriff sehr inflationär benutzt und ist eine häufige "Modediagnose".
 
Besonders anfällig für das Burn-Out-Syndrom sind Menschen in sozialen Berufen wie 
beispielsweise Gesundheits- und Kranken-, Alten-, Heilerziehungs-, Kinderpfleger, Erzieher, Lehrer, Therapeuten. 
Es betrifft also überwiegend die Berufe, die eng mit Menschen zusammenarbeiten, wobei die Übernahme der Verantwortung sehr einseitig ist. 
Es können  aber auch zum Beispiel Manager oder Arbeitslose betroffen sein.
 
Burn-Out ist eine schwerwiegende Stresserkrankung, die aber nicht von einem einzigen Stressfaktor 
verursacht wird, sondern durch eine Kombination von arbeitsbedingten und persönlichen Stressfaktoren. 
Man "brennt" nicht von einem Tag zum anderen "aus", sondern es ist ein schleichender Prozess, der in mehreren Stufen oder Phasen abläuft mit typischen Symptomen. 



Definition Burn-Out
=======================

Burn-Out  wurde  1974  vom  US amerikanischen  Psychoanalytiker  J.  Freudenberger  beschrieben.  Er  
bezeichnete  mit  Burnout  den  psychischen  und  physischen  Abbau  von  Mitarbeitern  sozialer  
Hilfsorganisationen,  die  in  therapeutischen  Wohngemeinschaften,  Frauenhäusern  oder  
Kriseninterventionszentren  gearbeitet  haben.

Hinzu  kam  die  Erscheinung  eines  vollständigen  Motivationsverlustes.  Später  wurde  der  
Begriff  auch  auf  andere  
Berufsgruppen  ausgeweitet und  erhielt  in  den  80er  
Jahren  auch  in  Deutschland  Aufmerksamkeit  in  den  Medien  und  in  Fachkreisen.


Freudenberger  (1974)  definiert  Burn-Out  als [#Freudenberger]_ ::

	„Nachlassen  bzw.  Schwinden  von  Kräften  oder  Erschöpfung  
	durch  übermäßige  Beanspruchung  der  eigenen  Energie,  
	Kräfte oder  Ressourcen“.  





.. [#Freudenberger] **Staff burn-out.**,Freudenberger H., Journal of Social Issues 1974; 30: 159 -165

Warum ausbrennen wir?
=======================

.. figure:: figs/don-quichote-und-windmuehle-7135353.jpg
   :align: center
   :width: 670px
   :height: 720px
   :scale: 100
   :alt: Burn-Out

von Dr. L. F. Satow [#Satow]_: 



::

        "Der jahrelange Kampf gegen Windmühlen, Stress, Missachtung und 
        Überarbeitung führen in die Erschöpfung. Oft auch, weil wir 
        die Warnsignale ignorieren."
  
  

An der Entstehung eines Burn-Out sind immer innere und 
äußere Faktoren beteiligt. Neben dem zunehmenden Stress im Arbeitsbereich 
beeinflusst auch das sogenannte Helfer-Syndrom den Ausbruch der Krankheit. 
Burn-Out-Betroffene waren anfänglich meist engagierte Mitarbeiter mit hohen Idealen. 
Denn nur wer entflammt war, kann ausbrennen. 
Dr. Wobrock glaubt [#Wobrock]_, dass der eigentliche Grund für diese fortdauernde 
Überarbeitung die Suche nach Anerkennung ist.
    
Die Hauptursachen für Burn-Out sind:
    
* Anfänglich besonderer Ehrgeiz und Idealismus 
* Fortdauernde Überarbeitung 
* Jahrelanger Stress bzw. falscher Umgang mit Stress (auch Mobbing) 
* Fehlende Anerkennung und berufliche Frustrationen 
* Persönliche Einstellungen und Dispositionen

.. Important::

   Angst vor Ablehnung ist die häufigste Ursache für Burnout.
   
   

.. [#Satow] **Die Webseite** http://www.burnout-syndrom-hilfe.eu/burnout-ursachen/
    
    
.. [#Wobrock]  **Burnout und Depressionen** - Interview mit Dr. Wobrock
               https://www.youtube.com/watch?v=auMiS0KldPU
               
    

   
               
----------------------------------------------------------------
Burn-Out: das Phasen-Modell (die Fortsetzung)
----------------------------------------------------------------

Man "brennt" nicht von einem Tag zum anderen "aus", sondern es ist ein schleichender Prozess, 
der in mehreren Stufen oder Phasen abläuft mit typischen Symptomen. 
Es gibt unterschiedliche Modelle des BurnOut-Syndroms mit drei bis 15 Phasen und da wiederum mit unterschiedlichen Bezeichnungen, 
was die Verwirrung perfekt macht.
Die amerikanischen Psychiater Jerry Edelwich und Archie Brodsky stellten 1984
ein Vier-Phasen-Modell vor [#Brodsky]_ [#Presinger]_.

.. [#Brodsky] **Stages of Disillusionment in the Helping Professions**, *J Edelwich, A Brodsky*,
              Human Sciences Press (1980)
              

|
|


.. figure:: figs/burnout_model.jpg
   :align: center
   :width: 1270px
   :height: 920px
   :scale: 100
   :alt: Burn-Out

|
|


Die Phasen sind:
    
1. **Idealistische Begeisterung**

   * Selbstüberschätzung
   * hochgesteckte Ziele
   * Optimismus 
   * hoher Energieeinsatz sowie Überidentifizierung mit der Arbeit

|

2. **Stillstand**

   * erste Enttäuschungen 
   * Beschränkung der Kontakte auf Kollegen, 
   * Reduzierung des Lebens auf die Arbeit 
   * Rückzug von Vernachlässigung des Familienlebens

|

3. **Frustration**

   * Erfahrung der Erfolg- und Machtlosigkeit 
   * Problemen mit Bürokratie 
   * gefühlter Mangel an Anerkennung von Kollegen und Vorgesetzten 
   * Gefühle von Inkompetenz 
   * psychosomatische Erkrankungen

|

4. **Apathie** 

   * zunehmende emotionale Entfremdung 
   * völlige Desillusionierung
   * Verzweiflung wegen schwindender beruflicher Alternativen 
   * Resignation 
   * Gleichgültigkeit gegenüber Kolleginnen und den früheren Arbeitszielen
  
|
|

Idealistische Begeisterung
=======================================

Von Burnout betroffene Menschen haben ihren Beruf meist sehr geliebt. 
Man ist zu Hochleistungen motiviert und fähig, weil man etwas bewirken und bewegen will. 
Meist handelt es sich um Berufe, bei denen man andere Menschen zu etwas motivieren will, 
zu etwas hinführen soll oder man für sie Verantwortung zu übernehmen hat.
Am Beginn steht die Leidenschaft für eine konkrete Aufgabe, die sehr viel Einsatz erfordert. 
In der ersten Phase ist von einer Beschränkung der Schaffenskraft noch nichts zu spüren.
Doch diese Zeit großer psychischer und emotionaler Anstrengung muss nicht zwingend in 
einen Burnout-Prozess münden. Entscheidend ist, ob es irgendwann gelingt, die 
fehlenden Erholungsphasen zuzulassen oder nicht.


|
|

Enttäuschung
================

Bleibt trotz des hohen Engagements nach subjektivem Empfinden der erhoffte Erfolg aus, 
wird das Ausbleiben des Erfolgs zunächst durch noch mehr Engagement ausgeglichen, um das Ziel 
doch noch zu erreichen. Dafür sorgt schon der eigene Anspruch an sich selbst. 
Damit verbunden ist oft ein Gefühl der Unentbehrlichkeit, das zugleich 
die einsetzende Hyperaktivität rechtfertigt.»Erholung« findet man am Abend nur mehr beim Glas Rotwein 
oder dem Zappen vor dem Fernseher. Die Wochenenden sind voller Arbeit und reichen nicht mehr aus, 
um die erforderliche Erholung zu ermöglichen. Die Energiebilanz ist ständig im Minus.

Wenn das hohe Engagement permanent keine Erfüllung und Entspannung in einer Zielerreichung findet, 
erlahmen die Kräfte immer mehr. Man verspürt schließlich zunehmend den Wunsch, sich von der Familie, 
Freunden und Kollegen zurückzuziehen. Der früher die Arbeitsleistung prägende Idealismus geht verloren, 
was man jedoch als erhöhte Reife und Lebensklugheit positiv zu deuten sucht. 
Müdigkeit, Gleichgültigkeit und Kraftlosigkeit machen sich breit. 

|
|

Frustration 
================

Je weiter und schneller sich die Burnout-Spirale dreht, desto mehr schreitet auch der Abbau 
von Kreativität und Motivation fort. Die Arbeit wird leidenschaftslos verrichtet, 
ohne jeglichen innovativen Impuls. Der Mensch funktioniert wie eine Maschine: 
keine Freude, keine Neugierde, keine Fröhlichkeit. 

Als Reaktion auf die Desillusionierung wird nun entweder sich selbst oder der Umwelt die Schuld für
den jetzigen Zustand zugewiesen. Im ersten Fall sind innere Abstumpfung, Fatalismus und Fluchtgedanken die Folge, 
im zweiten Fall reagiert der Betroffene aggressiv auf sein Umfeld: Reizbarkeit, Launenhaftigkeit und 
Intoleranz sind die wahrnehmbaren Konsequenzen.

Freunde ziehen sich in dieser Phase mehr und mehr zurück, der Betroffene wird einsam. 
Möglicherweise sieht er sich auch einem gezielten Mobbing ausgesetzt, zumal seine Arbeitsleistungen 
deutlich nachlassen und sein Auftreten auf Andere demotivierend wirkt.


|
|

Apathie 
================

Das ursprünglich nur zeitweilige Gefühl der Hilflosigkeit hat sich schließlich zu einem chronischen Gefühl der Hoffnungslosigkeit 
und Desillusionierung verdichtet. Das Leben verliert mehr und mehr seinen Sinn. Ein Gefühl des völligen 
Versagens füllt den Menschen aus. Die Seele erstarrt, 
ja es entwickelt sich ein regelrechter Widerwillen gegen sich selbst (Depersonalisation).

Am Ende der Burn-Out-Spirale steht eine völlige Umwertung der eigenen Werte und eine tiefgreifende 
Änderung der Persönlichkeit: die Lebensprioritäten haben sich geändert, und damit verbunden entwickelt
sich eine negative Einstellung zum Leben (fehlende Lebensperspektive), zur Arbeit (Zweifel am Sinn der 
Tätigkeit) und zu sich selbst (Selbstzweifel).


|
|

Burn-Out: Wer ist besonders betroffen?
========================================

Burn-Out ist heimtückisch. Er trifft keinesfalls nur die Schwachen, die Labilen, die Jammerlappen 
und Weicheier. Betroffen sind ebenso Manager, Menschen in sozialen Berufen, Beschäftigte in 
sogenannten Sandwichpositionen, also zwischen zwei Hierarchieebenen, Schichtarbeiter und Berufspendler. 
Aber auch Menschen, die unter dem Druck stehen, ständig ihre Emotionen im Zaum zu halten und nach 
außen eine neutrale Fassade zu wahren, erkranken häufiger an einem Burnout als andere.

Laut den Forschern von der Psychologen gehören zu den besonders Gefährdeten:


* Journalisten
* Krankenhausmitarbeiter (überhaupt Angestellte im Gesundheitssystem)
* Sozialarbeiter
* Anwälte
* Servicemitarbeiter

Interessanterweise sind laut Statistiken auch Frauen stärker gefährdet als Männer. 
Sie neigen vermehrt dazu, es allen recht machen zu wollen, und stützen ihr Selbstwertgefühl 
öfter auf äußere Anerkennung.


Am größten ist die Gefahr, einen Burn-Out zu erleiden, im Alter zwischen 30 und 49 Jahren.


Schätzungsweise 4% der Bevölkerung leiden unter schweren Burn-Out-Symptomen. 
Man geht aber davon aus, dass weitere 30% unter mittelschweren Symptomen leiden, 
die häufig klinisch nicht in Erscheinung treten.

|
|
|

Was heißt ein Burn-Out-Fall für die Familie?
=====================================================

Nicht nur Manager können am Burn-Out-Syndrom erkranken sondern auch Eltern - egal ob berufstätig oder 
nicht. Stress im Familienleben kann bei Menschen zum Ausbrennen und zur totalen 
Erschöpfung führen. 

::

 "Ständig unter Strom stehen, immer abrufbereit auf die Familien reagieren - da ist irgendwann der 
  Akku leer!" 

:math:`\leftarrow`  So oder ähnliche beschreiben Familien im Burn-Out ihr Gefühl.
Jede Kleinigkeit wird zur unüberwindbaren Belastung. 

Kinder reagieren sensibel auf solche Veränderungen. 
Sie ziehen sich entweder zurück weil die betroffenen Eltern so gereizt sind, oder versuchen mit 
Ungezogenheiten die Aufmerksamkeit der Eltern zurück zu gewinnen. 
Die Kinder werden ernster und vermeiden es, fröhlich zu sein.

Die Psychologen herausgefunden haben, 
dass Kinder von Eltern, die an Burnout erkrankt sind, häufiger selbst an schulischem 
Burn-Out erkranken.
   






.. [#Presinger] **BURNOUT**, Die Vorlesung von Dr. Susanne Presinger
                http://www.psychosomatik.at/uploads/lexikon_pdf/burnout.pdf


|
|

----------------------------------------------------------------
So vermeiden wir Burn-Out
----------------------------------------------------------------

Sitzen Sie auch in der Stressfalle? Oder fühlen Sie sich gefährdet und 
wollen es nicht bis zum Burn-Out kommen lassen?

Zur Diagnose und Therapie ist professionelle medizinisch-therapeutische Hilfe 
jedoch unabdingbar, da es sich um eine ernstzunehmende Erkrankung handelt, 
die zu ernsthaften Gesundheitsschäden führen kann. Eine frühzeitige Behandlung 
ist deshalb sehr wichtig, da die Übergänge zu Depressionen und anderen psychischen 
Erkrankungen wie z.B. Schmerzsyndromen fließend sind.

|
|

12 goldene Regeln von Dr. Vinzenz Mansmann [#Mansmann]_ 
==============================================================



Dr. Vinzenz Mansmann, langjähriger Anti-Stress-Experte, beschreibt 12 goldene Regeln, 
um Burnout  verhindern und selbst behandeln zu können:
    
1. **Verleugnen ist Tabu.**

|

2. **Lebensumstände verändern.**

|
    
3. **Überengagement vermeiden.**

|

4. **Isolation vermeiden.**

|


5. **Schluss mit Überfürsorglichkeit.**

|

6. **Kürzer treten.**

|

7. **Wertewechsel.**

|

8. **Der Mut zum "Nein".**

|


9. **Persönliches Tempo.**

|

10. **Achtung Körper!**

|


11. **Umgang mit Sorgen und Ängsten.** 

|


12. **Behalten Sie Ihren Sinn für Humor.**


|
|
|

Behandlungsbausteine der Therapie
============================================

Welche Behandlung kommt bei Burn-Out infrage? Gegen die Probleme helfen Verhaltensänderungen, 
Psychotherapie und Medikamente – je nach Fall einzeln oder in Kombination.

Für das Burn-Out-Syndrom gibt es keine Standard-Therapie. 
Die Behandlung muss immer zum Patienten und seiner Lebenssituation passen. 
In leichten Fällen kann es genügen, die eigene Lebens- und Arbeitssituation zu überdenken 
und unter Anleitung etwas "Ordnung" in den Alltag zu bringen.
Psychotherapie, insbesondere Verhaltenstherapie, hat sich bei deutlicheren 
Burn-Out-Symptomen als hilfreich erwiesen. 
Die Behandlung kann ambulant, oder falls nötig auch in einer Klinik stattfinden. 
Ziel ist es, die negativen Gedanken durch positive zu ersetzen, das eigene Selbstbewusstsein 
zu stärken. Patienten lernen zum Beispiel, ihre Gefühle deutlicher wahrzunehmen und auszudrücken.
Wenn die depressive Symptomatik stärker ausgeprägt ist und eine konstruktive Bearbeitung der Problemsituation erschwert, 
kann der Arzt eventuell auch Medikamente verschreiben.
Diese Medikamente  werden auch gegen Depressionen verschrieben.


Alle zusammen sind die Behandlungen des Burn-Out:
    


* Medizinische Versorgung der bestehenden körperlichen Beschwerden

|

* Psychoedukation wie Stressbewältigung , Umgang mit beruflichen Anforderungen

|

* Persönliche Beratung / Coaching / Therapie

|

* Erlernen von Entspannungsverfahren

|

* Förderung der Kreativität

|

* Körperliche Aktivierung in Form von sportlicher Betätigung


.. raw:: pdf

   PageBreak cutePage
   
   
   
------------------------------------
Sind Sie von Burnout bedroht?
------------------------------------

Wenn Sie die folgenden Fragen beantworten, können Sie sich Ihren Gefährdungsgrad [#PhysHeute]_ 
für Burnout errechnen. Mit seiner Hilfe können Sie feststellen, wie Sie Ihre Arbeit oder 
Ihr Leben empfinden, wie Sie sich im allgemeinen oder
auch nur an diesem Tag fühlen. Bitte beantworten Sie nach der folgenden Skala, ob Sie

.. [#PhysHeute]  **Psychologie heute**, 10/1983

|
|
|


.. figure:: figs/test1.jpg
   :align: center
   :width: 1870px
   :height: 1370px
   :scale: 100
   :alt: Burn-Out

|
|


.. [#Mansmann] **Total erschöpft: Neue Energie mit Naturmedizin**, Dr. med. Vinzenz Mansmann, ISBN-13: 978-3925868085


.. raw:: pdf

   PageBreak cutePage
   
   


.. figure:: figs/test2.jpg
   :align: center
   :width: 1870px
   :height: 1370px
   :scale: 100
   :alt: Burn-Out

----------------------------------------------------------------
Literaturhinweise
----------------------------------------------------------------


    
.. target-notes::


